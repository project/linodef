
/**
 * @file
 * Create the Linodef button with Javascript.
 * Based on default BUEditor function eDefTagChooser() in default_buttons_functions.js by ufku.
 */

// Create clickable tag options that insert corresponding tags into the editor. [[tag, title, attributes],[...],...]
function eDefTagChooser_linodef(tags, applyTag, wrapEach, wrapAll, effect) {
  var content = '';
  var choice = eDefHTML(wrapEach, '<a href="javascript:void(0)" class="choice" onclick="eDefClickChoice_linodef(\'%tag\', \'%esc\')">%html</a>') +'\n';
  for (var i in tags) {
    var html = eDefHTML(tags[i][0], tags[i][1], tags[i][2]);
    content += choice.replace('%html', applyTag ? html : tags[i][1]).replace('%tag', tags[i][0]).replace('%esc', escape(html));
  }
  BUE.quickPop.open(eDefHTML(wrapAll, content, {'class': 'chooser'}), effect);
}
function eDefClickChoice_linodef(tag, html) {
  var html = unescape(html), partA = html.substr(0, html.indexOf('>')+1), E = BUE.active;
  E.replaceSelection(tag);
  E.focus();
}