﻿// $Id$

Linodef - BUEditor
------------------
This module provides functions to create buttons that uses the capabilities of the
Linodef filter.

Go to Administer -> Site building -> Modules and enable the "Linodef - BUEditor" module.

Requirements
------------
  - Linodef
  - BUEditor
  - Advanced help - to access the help documentation (optional)

Usage
-----
To use the buttons import and customize the button found at linodef_bueditor_button.csv
(included at modules/linodef/modules/linodef_bueditor) like you do it with other custom
buttons.
To get detailed information refer to your editors manual or use the included help
documentation. Advanced help module required.