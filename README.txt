﻿// $Id$

Linodef - Link nodes & terms and embed fields & terms
-----------------------------------------------------
To install, place the entire Linodef folder into your modules directory.
Then go to Administer -> Site building -> Modules and enable the "Linodef"
module.

Requirements
------------
  - Drupal 6
  - CCK - to embed field values
  - Textfield - to embed field values
  - Taxonomy - to embed terms
  - Advanced help - to access the help documentation (optional)

Usage
-----
Use it like any other filter.
Go to Administer -> Settings -> Input types and activate the Filter
"Linodef" for the input type of your choice.
Now you can use the tags as described at the input type tips page:
/filter/tips.

To get more information, e.g. about the provided editor API use the
included help documentation. For that you need Advanced help module.