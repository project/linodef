<?php

/**
 * @file
 * The replace functions executed in the process filter step.
 *
 * @package Linodef
 * @author Roi Danton
 */

/**
 * Return body with replaced tags.
 *
 * Search and replace the tags with internal links and node title or field values.
 *
 * @param $body
 *   Text on that the preg_replace function will be executed.
 */
function _linodef_filter_process(&$body) {

  // Define Linodef tags
  $preg = array(

    // Link Node ID with user defined text
    "@\xf4\x80\x81\x8c#([0-9]+)\xf4\x80\x8f\x8d(.*?)\xf4\x80\x81\x8c/#\xf4\x80\x8f\x8d@se"                => "_linodef_find_node($1,'$2');",

    // Link Node ID and embed title or field values
    "@\[#([0-9]+)\]@e"                                      => "_linodef_find_nodesnfields($1);",
    "@\[#([0-9]+):([0-9a-z_]+)\]@e"                         => "_linodef_find_nodesnfields($1,$2);",
    "@\[#([0-9]+):([0-9a-z_]+):([0-9]+)\]@e"                => "_linodef_find_nodesnfields($1,$2,$3);",

    // Embed title or field values, add no link
    "@\[#nl([0-9]+)\]@e"                                    => "_linodef_find_nodesnfields($1,'title',0,True);",
    "@\[#nl([0-9]+):([0-9a-z_]+)\]@e"                       => "_linodef_find_nodesnfields($1,$2,0,True);",
    "@\[#nl([0-9]+):([0-9a-z_]+):([0-9]+)\]@e"              => "_linodef_find_nodesnfields($1,$2,$3,True);",

    // Link Node ID and embed vocabulary term -> not senseful, termID does this better -> removed
    // Link term ID and embed term name
    "@\[#t([0-9]+)\]@e"                                     => "_linodef_find_term($1);",
  );
  $body = preg_replace(array_keys($preg), array_values($preg), $body);

  return $body;
}

/**
 * Helper function for _linodef_filter_process.
 *
 * @param $nid
 *   Node ID of the node that will be linked.
 * @param string $text
 *   The user entered text.
 * @return
 *   Returns a string containing the link or a hint if node not found.
 */
function _linodef_find_node($nid, $text) {
  if ($nid && $node = node_load($nid)) {
    if (node_access('view', $node)) {
        // l() does check_plain on $text and does any path aliases if required (thx @AjK).
        return l($text, 'node/'. $nid);
    }
    else {
        // Access denied not needed because the user without required rights won't see any content from the node he has no access to.
        return check_plain($text);
    }

  }
  return linodef_message(array('nid' => $nid), 'not found') .', '. t('your description: @UserText', array('@UserText' => $text));
}

/**
 * Helper function for _linodef_filter_process.
 *
 * @param $nid
 *   Node ID of the node that will be linked.
 * @param $fieldname
 *   The name of the field reading the value of or "title" (to include nodetitle) if not set.
 * @param $valuenumber
 *   Number to get the desired value of multiple value fields or 0 if not set.
 * @param $no_link
 *   If true no link will be added to the embedded node title or field value.
 * @return
 *   Returns a string containing the (linked) node title/field value or a hint if node/field/field-content not found.
 */
function _linodef_find_nodesnfields($nid, $fieldname = 'title', $valuenumber = 0, $no_link = False) {
  if ($nid && $node = node_load($nid)) {
    if (node_access('view', $node)) {
        if ($fieldname == 'title') {
            $output = $node->title;
        }
        elseif (property_exists($node, $fieldname)) {
            $output = get_object_vars($node);
            $output = $output[$fieldname][$valuenumber]['value'];
            if (drupal_strlen($output) < 1) {
                if ($valuenumber == 0) {
                    return t('%Field has no content.', array('%Field' => $fieldname));
                }
                else {
                    return t('Value %ValueN of %Field not found or has no content.', array('%ValueN' => $valuenumber, '%Field' => $fieldname));
                }
            }
        }
        else {
            return linodef_message(array('field' => $fieldname), 'not found');
        }
        if ($no_link) {
            return check_plain($output);
        }
        else {
            // l() does check_plain on $text and does any path aliases if required (thx @AjK).
            return l($output, 'node/'. $nid);
        }
    }
    else {
        // Access denied needed otherwise imagine the following: If a user writes a comment and tries to include e.g. [#NodeID] he would be able to see the title even if he have no proper access rights.
        return linodef_message(array('nid' => $nid), 'access denied');
    }
  }
  else {
    return linodef_message(array('nid' => $nid), 'not found');
  }
}

/**
 * Helper function for _linodef_filter_process.
 *
 * @param $tid
 *   Term ID of the term that will be linked and embedded.
 * @return
 *   Returns a string containing the linked term name or a hint if term not found.
 */
function _linodef_find_term($tid) {
    $term = taxonomy_get_term($tid);
    if ($term) {
        $termlink = taxonomy_term_path($term);
        return l($term->name, $termlink);
    }
    else {
        return linodef_message(array('tid' => $tid), 'not found');
    }
}