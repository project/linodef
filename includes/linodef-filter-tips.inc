<?php

/**
 * @file
 * The help text for Linodef, used in main module file.
 */

/**
 * Help text.
 *
 * @return
 *   Text with html markup.
 */
function _linodef_filter_tips() {
  global $base_url;
  $output  = '<a name="linodef"><h2><strong>'. t('Linodef') .'</strong></h2></a>';
  $output .= '<p>'. t("%Linodef is a filter for internal links. It uses a simple but unique markup so %Linodef doesn't interfere with other filter modules.", array('%Linodef' => 'Linodef')) .'<br />';
  $output .= t("The ID of a node or of a taxonomy term and optionally a fieldname are the solely elements stored in your text. One advantage of this method in comparison to similar filter modules is the fact that the links don't break if you change the node title or term name. Furthermore, if you change the title of the node or the value of a field or the name of a term then those are automatically updated in your text, too.") .'</p>';
  $output .= '<p>'. t('Node links can be embedded in two ways, term links in one:') .'</p>';

  // Way 1: Embedding title & fields.
  $output .= '<h3 style="text-decoration: underline;">'. t('Embed title or a field value with link to the corresponding node') .'</h3>';
  $output .= '<p>'. t("To embed field values or node titles, surround the node ID by brackets [] and the hash key #: [#NodeID]. Optionally you may insert a fieldname to embed the field value into the text instead of the node's title. Moreover it is possible to add a field value number in the case you have a field with multiple values and want to use a different value than the first.") .'</p>';
  $output .= '<p>'. t('The syntax is as following:') .'</p>';
  // Syntax table.
  $tips = array(
    array('name' => 'nid',                  'desc' => t('Embed node title and link to the node.'), 'type' => '[#NodeID]', 'get' => '<a href="'. $base_url .'/node/NodeID">'. t('node title') .'</a>'),
    array('name' => 'nid_field',            'desc' => t('Embed field value and link to the node.'), 'type' => '[#NodeID:field_name]', 'get' => '<a href="'. $base_url .'/node/NodeID">'. t('field value') .'</a>'),
    array('name' => 'nid_field_multiple',   'desc' => t('Embed field value and link to the node.'), 'type' => '[#NodeID:field_name:field_value_number]', 'get' => '<a href="'. $base_url .'/node/NodeID">'. t('certain field value of a multiple values field') .'</a>'),
    array('name' => 'nid_nl',               'desc' => t('Embed field value or node title but create no link.'), 'type' => '[#nl...]', 'get' => t('field value') .' '. t('or') .' '. t('node title'))
  );
  $output .= _linodef_filter_tips_maketable($tips);
  // Example
  $output .= '<h4>'. t('Example') .':</h4>';
  $output .= t('We have a node with id=!id and a title %title. It contains a multiple value field called !firstnames. The first value is %value1, second is %value2. Note that the field value numbers start at 0, so first value has number &quot;0&quot;, second has number &quot;1&quot; etc. If the node has a path alias then the link contains this instead of the default drupal path (the examples are limited to the default paths).', array('!id' => '<strong>8</strong>', '!firstnames' => '<em>firstnames</em>', '%title' => 'Perry Rhodan', '%value1' => 'Gucky', '%value2' => 'Atlan'));
  // Example table
  $tips = array(
    array('name' => 'nid',                      'type' => '[#8]', 'get' => '<a href="'. $base_url .'/node/8">Perry Rhodan</a>'),
    array('name' => 'nid_field',                'type' => '[#8:field_firstnames]', 'get' => '<a href="'. $base_url .'/node/8">Gucky</a>'),
    array('name' => 'nid_field_multiple',       'type' => '[#8:field_firstnames:0]', 'get' => '<a href="'. $base_url .'/node/8">Gucky</a>'),
    array('name' => 'nid_field_multiple',       'type' => '[#8:field_firstnames:1]', 'get' => '<a href="'. $base_url .'/node/8">Atlan</a>'),
    array('name' => 'nid_nl',                   'type' => '[#nl8]', 'get' => 'Perry Rhodan'),
    array('name' => 'nid_field_multiple_nl',    'type' => '[#nl8:field_firstnames:1]', 'get' => 'Atlan'),
  );
  $output .= _linodef_filter_tips_maketable($tips);

  // Way 2: Link nodes.
  $output .= '<h3 style="text-decoration: underline;">'. t('Create node links with own linktext') .'</h3>';
  // Syntax
  $output .= '<p>'. t('The syntax is as following:') .'<br />&lt;#NodeID&gt;'. t('your Text') .'&lt;/#&gt;.</p>';
  // Example
  $output .= '<h4>'. t('Example') .':</h4>';
  // Example table
  $tips = array(
    array('name' => 'nidtag',               'type' => '&lt;#8&gt;'. t('your Text') .'&lt;/#&gt;', 'get' => '<a href="'. $base_url .'/node/8">'. t('your Text') .'</a>'),
  );
  $output .= _linodef_filter_tips_maketable($tips);

  // Way 3: Link & embed terms.
  $output .= '<h3 style="text-decoration: underline;">'. t('Embed taxonomy term and link to it') .'</h3>';
  // Syntax
  $output .= '<p>'. t('The syntax is as following:') .'<br />[#tTermID].</p>';
  // Example
  $output .= '<h4>'. t('Example') .':</h4>';
  $output .= t('We have a taxonomy term with id=!id and the name %termname. If the term has a path alias then the link contains this instead of the default drupal path (the example is limited to the default path).', array('!id' => '<strong>5</strong>', '%termname' => 'Jupiter'));
  // Example table
  $tips = array(
    array('name' => 'tid',                  'type' => '[#t5]', 'get' => '<a href="'. $base_url .'/taxonomy/term/5">Jupiter</a>'),
  );
  $output .= _linodef_filter_tips_maketable($tips);
  return $output;
}

/**
 * Create formatted table of an array using similar layout as core filter module.
 *
 * If $tips has no desc, only two columns are used.
 *
 * @param array $tips
 *   Array with type, get (and desc)
 * @return
 *   Returns the table with appropriate html markup.
 */
function _linodef_filter_tips_maketable($tips) {
  for ($i = 0; $i < count($tips); $i++) {
    if ($tips[$i]['desc']) {
        $rows[] = array(
            array('data' => $tips[$i]['desc'], 'class' => 'description'),
            array('data' => '<code>'. $tips[$i]['type'] .'</code>', 'class' => 'type'),
            array('data' => '<code>'. $tips[$i]['get'] .'</code>', 'class' => 'get')
        );
    }
    else {
        $rows[] = array(
            array('data' => '<code>'. $tips[$i]['type'] .'</code>', 'class' => 'type'),
            array('data' => '<code>'. $tips[$i]['get'] .'</code>', 'class' => 'get')
        );
    }
  }
  if ($tips[0]['desc']) {
    $header = array(t('Tag Description'), t('You Type'), t('You Get'));
  }
  else {
    $header = array(t('You Type'), t('You Get'));
  }
  return theme('table', $header, $rows);
}