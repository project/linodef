<?php

/**
 * @defgroup linodef_editorbuttons_api Linodef Editorbuttons API
 *
 * @file
 * Retrieve the data for Linodef submodules that adds (or provides functions for) contrib editor buttons.
 *
 * Additionally user data is filtered with check_plain and newlines are removed for compatibility with javascript of editors.
 *
 * @package Linodef
 * @author Roi Danton
 * @ingroup linodef_editorbuttons_api
 */

/**
 * Choose which method to use to get the data.
 *
 * Furthermore here the string is set for the tag which includes no links. Show terms by contenttype removed as user should use vid instead.
 *
 * @param string $drupal_element_type
 *      The element type to retrieve the data from. This has to be a supported element type. See Linodef advanced help.
 * @param $no_link
 *      If true, a "no link" marker will be added to the tag, so the embedded node title or field value are not linked.
 * @return
 *      array with data
 *          - tag: the tag to be inserted
 *          - desc: the description for the editor button
 *      string with error message
 *
 * @see _linodef_buttons_getfieldsbyfieldname(), _linodef_buttons_gettermsbyvid(), _linodef_buttons_getnodesbycontenttype()
 */
function linodef_editorbuttons_switch($drupal_element_type, $no_link = False) {
    if ($no_link) {
        $no_link = 'nl';
    }
    if (substr($drupal_element_type, 0, 6) == "field_") {
        $output = _linodef_buttons_getfieldsbyfieldname($drupal_element_type, $no_link);
    }
    elseif (preg_replace("@([0-9]+)@", "VID", $drupal_element_type) == "VID") {
        $output = _linodef_buttons_gettermsbyvid($drupal_element_type);
    }
    else {
        $output = _linodef_buttons_getnodesbycontenttype($drupal_element_type, $no_link);
    }
    return $output;
}

/**
 * Get data for button: Show fields by field name.
 *
 * @param string $fieldname
 *      The name of the field
 * @return
 *      array with node/field data:
 *          - tag: the tag to be inserted
 *          - desc: the description for the editor button
 *      string
 *          if node/field (value) not found a message is returned which should appear in editor text field (depends on Linodef submodule for editor)
 *
 * @see linodef_editorbuttons_switch()
 */
function _linodef_buttons_getfieldsbyfieldname($fieldname, $no_link) {
    $field = content_fields($fieldname);
    $fieldvalues = content_database_info($field);
    if ($fieldvalues['columns']['value']) {
        $output = array();
        $i = 0;
        $result = db_query('SELECT n.%s as value, n.* FROM {%s} n', $fieldvalues['columns']['value']['column'], $fieldvalues['table']);
        while ($ergebnis = db_fetch_object($result)) {
            // Check if field has a value. If not, it doesn't appear.
            if ($ergebnis->value) {
                if ($ergebnis->nid && $node = node_load($ergebnis->nid)) {
                    if (node_access('view', $node)) {
                        // Security step.
                        $ergebnis->value = check_plain($ergebnis->value);
                        $node->title = check_plain($node->title);
                        // Check if user wants to have a no link tag.
                        if ($no_link) {
                            $ergebnis->nid = $no_link . $ergebnis->nid;
                        }
                        // Make compatible with editor's Javascripts (remove newline).
                        $ergebnis->value = str_replace(array("\r\n", "\n", "\r"), ' ', $ergebnis->value);
                        // Multiple values: only insert valuenumber if fieldvalue is not first value.
                        if ($ergebnis->delta && $ergebnis->delta > 0) {
                            $output[$i] = array('tag' => "[#". $ergebnis->nid .":". $fieldname .":". $ergebnis->delta ."]", 'desc' => $ergebnis->value ."(". $node->title .")");
                        }
                        else {
                            $output[$i] = array('tag' => "[#". $ergebnis->nid .":". $fieldname ."]", 'desc' => $ergebnis->value ."(". $node->title .")");
                        }
                    }
                    else {
                        $output[$i] = array('tag' => "", 'desc' => linodef_message(array('nid' => $ergebnis->nid), 'access denied'));
                    }
                    $i++;
                }
            }
        }
        return $output;
    }
    elseif ($fieldvalues['columns']['nid']) {
        return t('Reference fields are not supported.') .' '. linodef_message(array('elementtypes' => 'true')) .' '. t('Instead of this fieldname the usage of a content type is recommended.');
    }
    else {
        return linodef_message(array('field' => $fieldname), 'not found') . t('or unsupported field type.') .' '. linodef_message(array('elementtypes' => 'true')) .' '. linodef_message(array('checkinputvar' => '$drupal_element_type'));
    }
}

/**
 * Get data for button: Show nodes by content type
 *
 * @param string $contenttype
 *      The content type to get the nodes from.
 * @return
 *      array with node data:
 *          - tag: the tag to be inserted
 *          - desc: the description for the editor button
 *      string
 *          if content type not found a message is returned which should appear in editor text field (depends on Linodef submodule for editor)
 *
 * @see linodef_editorbuttons_switch()
 */
function _linodef_buttons_getnodesbycontenttype($contenttype) {
    if (node_get_types('type', $contenttype) ) {
        $output = array();
        $i = 0;
        $result = db_query('SELECT n.nid FROM {node} n WHERE n.type = "%s"', $contenttype);
        while ($ergebnis = db_fetch_object($result)) {
            if ($ergebnis->nid && $node = node_load($ergebnis->nid)) {
                if (node_access('view', $node)) {
                    $node->title = check_plain($node->title);
                    // Check if user wants to have a no link tag.
                    if ($no_link) {
                        $node->nid = $no_link . $node->nid;
                    }
                    $output[$i] = array('tag' => "[#". $node->nid ."]", 'desc' => $node->title);
                }
                else {
                    $output[$i] = array('tag' => "", 'desc' => linodef_message(array('nid' => $ergebnis->nid), 'access denied'));
                }
                $i++;
            }
        }
        return $output;
    }
    else {
        return linodef_message(array('ctype' => $contenttype), 'not found') .'. '. linodef_message(array('checkinputvar' => '$drupal_element_type'));
    }
}

/**
 * Get data for button: Show terms by vocabulary.
 *
 * @param string $vid
 *      The ID of the vocabulary to get the terms from.
 * @return
 *      array with node data:
 *          - tag: the tag to be inserted
 *          - desc: the description for the editor button
 *      string
 *          if vid not found a message is returned which should appear in editor text field (depends on Linodef submodule for editor)
 *
 * @see linodef_editorbuttons_switch()
 */
function _linodef_buttons_gettermsbyvid($vid) {
    if ($terms = taxonomy_get_tree($vid)) {
        $output = array();
        for ($i = 0; $i < count($terms); $i++) {
            $output[$i] = array('tag' => "[#t". $terms[$i]->tid ."]", 'desc' => $terms[$i]->name);
        }
        return $output;
    }
    else {
        return linodef_message(array('vid' => $vid), 'not found') .'. '. linodef_message(array('checkinputvar' => '$drupal_element_type'));
    }
}