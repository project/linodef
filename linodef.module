<?php

/**
 * @defgroup linodef Linodef: Link nodes and embed fields, link and embed terms
 *
 * This module contains several include files. There is an api for submodules which implements buttons in various editors to facilitate the creation of buttons which supports Linodef filter.
 */

/**
 * @file
 * The main Linodef filter module with filter hooks.
 *
 * Linodef is a filter which filters ID tags, add links to nodes and embeds field values.
 * 
 * @package Linodef
 * @author Roi Danton
 */

/**
 * Implementation of hook_filter.
 */
function linodef_filter($op, $delta = 0, $format = -1, $text = '') {
  if ($op == 'list') {
    return array(
      0 => t('Linodef - Link nodes & taxonomy terms and embed field values & term names.')
    );
  }

  // Provides extensibility.
  switch ($delta) {

    case 0:

      switch ($op) {
        case 'description':
          return t('Substitutes the Node ID for the node title or field value & links to this node. Furthermore Linodef substitutes the term ID for the term name & links to the term.');

        // Since lineodef will return a different result on each page load, we
        // need to return TRUE for "no cache" to ensure that the filter is run
        // every time the text is requested.
        case 'no cache':
          return TRUE;

        // I tried to use the bytes 0xFE and 0xFF to replace < and > here. These bytes
        // are not valid in UTF-8 data and thus unlikely to cause problems.
        // This doesnt work with double quotes "", but we need this in order to execute
        // the second preg_replace in filter.inc, so valid bytes U+10004C & U+1003CD
        // have been used.
        case 'prepare':
          return preg_replace('@<#([0-9]+)>(.*?)</#>@s', "\xf4\x80\x81\x8c#$1\xf4\x80\x8f\x8d$2\xf4\x80\x81\x8c/#\xf4\x80\x8f\x8d", $text);

        case 'process':
          include_once(drupal_get_path('module', 'linodef') .'/includes/linodef-filter.inc');
          return _linodef_filter_process($text, $format);
      }
      break;
  }
}

/**
 * Implementation of hook_filter_tips.
 */
function linodef_filter_tips($delta, $format, $long = FALSE) {
  switch ($delta) {
    case 0:
      if ($long) {
        include_once(drupal_get_path('module', 'linodef') .'/includes/linodef-filter-tips.inc');
        return _linodef_filter_tips();      
      }
      else {
        return t('Embeds node title or field values by node ID and terms by term ID (<a href="@format-tips-page">[#8], [#8:field_name], [#8:field_name:2], &lt;#8&gt;Your Text&lt;/#8&gt;, [#t8]</a>).', array('@format-tips-page' =>  url('filter/tips/'. $format, array('fragment' => 'linodef'))));
      }
      break;
  }
}

/**
 * Returns default messages that are needed several times and thus should be identical.
 *
 * @param array $type
 *    - keys: nid, field or tid etc
 *    - values: the associated value ($nid, $fieldname, $tid)
 *
 * @return string
 *   Returns a message.
 */
function linodef_message($type, $message = FALSE) {
    if ($type['nid']) {
        $context = t('Node') .' '. $type['nid'];
    }
    elseif ($type['field']) {
        $context = t('Field') .' '. $type['field'];
    }
    elseif ($type['tid']) {
        $context = t('Term') .' '. $type['tid'];
    }
    elseif ($type['ctype']) {
        $context = t('Content type') .' '. $type['ctype'];
    }
    elseif ($type['vid']) {
        $context = t('Vocabulary') .' '. $type['vid'];
    }
    elseif ($type['checkinputvar']) {
        $output = t('Check value for %inputvariable or ask your site administrator.', array('%inputvariable' => $type['checkinputvar']));
    }
    elseif ($type['elementtypes']) {
        $output = t('Supported fields must store a (single or multiple) value such as textfields & datefield (from date).');
    }
    else {
        $output = t('Unknown key for $type in linodef_message().');
    }
    if ($context && $message) {
        switch ($message) {
        case "access denied":
            // drupal_access_denied() doesn't fit here because no page should be returned and the pagetitle should not show "Access denied"
            $output = t('Access to !context denied.', array('!context' => $context));
            break;
        case "not found":
            $output = t('!context not found', array('!context' => $context));
            break;
        }
    }
    return $output;
}